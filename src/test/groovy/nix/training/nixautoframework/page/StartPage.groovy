package nix.training.nixautoframework.page

import geb.Page

class StartPage extends Page {

    static content = {
        blogLink (wait: true) { $("a", text: "Blog")}
    }

    static at = {
        title == "NIX – Outsourcing Offshore Software Development Company"
    }

    def "User clicks on Blog page"(){
        blogLink.click()
    }
}
