package nix.training.nixautoframework.spec

import geb.spock.GebReportingSpec
import nix.training.nixautoframework.page.BlogPage
import nix.training.nixautoframework.page.StartPage

class NixNavigationSpec extends GebReportingSpec{

    def "Navigate to Blog page"(){
        when:
            to StartPage

        and:
            "User clicks on Blog page"()

        then:
            at BlogPage
    }
}
